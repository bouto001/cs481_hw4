﻿using RainbowSixWiki.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RainbowSixWiki.TabbedPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WeaponsPage : ContentPage
    {
        ObservableCollection<Weapons> weapons = new ObservableCollection<Weapons>();
        public ObservableCollection<Weapons> Weapons { get { return weapons; } }

        public WeaponsPage()
        {
            InitializeComponent();
            WeaponsView.ItemsSource = weapons;

            InitializeWeapons();

            BindingContext = Weapons;
            OnPropertyChanged(nameof(Weapons));
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            await DisplayAlert("Weapons", "This is a list of weapons featured in Tom Clancy's Rainbow Six Siege.", "Got it");
        }

        #region Weapon initialisation
        public void InitializeWeapons()
        {
            weapons.Add(new Weapons
            {
                Name = "R4-C",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/9/97/R4c-image.jpg/revision/latest/scale-to-width-down/310?cb=20151209031900",
                Details = "The R4-C is an assault rifle featured in Tom Clancy's Rainbow Six Siege. It is available for use by FBI SWAT Recruits and the Operator Ash."
            });
            
            weapons.Add(new Weapons
            {
                Name = "F2",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/0/0f/F2-image.jpg/revision/latest/scale-to-width-down/310?cb=20151209034219",
                Details = "The F2 is an assault rifle featured in Tom Clancy's Rainbow Six Siege. It is available for use by GIGN Recruits and the Operator Twitch."
            });
            
            weapons.Add(new Weapons
            {
                Name = "FMG-9",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/5/58/Fmg9-image.jpg/revision/latest/scale-to-width-down/310?cb=20151210204101",
                Details = "The FMG-9 is a submachine gun featured in Tom Clancy's Rainbow Six Siege. It is available for use by the SAS Recruits, the SAS Operator Smoke and the Jaeger Corps Operator Nøkk"
            });
            
            weapons.Add(new Weapons
            {
                Name = "T-5 SMG",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/f/f4/R6S-t5smg.jpg/revision/latest/scale-to-width-down/310?cb=20170829173729",
                Details = "The T-5 SMG is a submachine gun featured in Tom Clancy's Rainbow Six Siege. It was introduced in the Operation Blood Orchid expansion pack and is available for use by the Operator Lesion."
            });
            
            weapons.Add(new Weapons
            {
                Name = "MP5",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/8/88/Mp5mli-image.jpg/revision/latest/scale-to-width-down/310?cb=20151209005123",
                Details = "The MP5 is a submachine gun featured in Tom Clancy's Rainbow Six Siege. It is available for use by GIGN Recruits, Operators Doc and Rook and the upcoming Operator Oryx."
            });
            
            weapons.Add(new Weapons
            {
                Name = "PRB92 - \"Luison\"",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/4/4f/R6S-prb92.jpg/revision/latest/scale-to-width-down/310?cb=20160806040008",
                Details = "The PRB92 along with its unique variant, the \"Luison\", is a weapon featured in the Operation Skull Rain expansion pack of Tom Clancy's Rainbow Six Siege. The PRB92 is available for use by the Operator Capitão, while the \"Luison\" is exclusive to the Operator Caveira."
            });

        }

        #endregion
    }
}