﻿using RainbowSixWiki.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RainbowSixWiki.TabbedPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OperatorsPage : ContentPage
    {
        ObservableCollection<Operators> operators = new ObservableCollection<Operators>();
        public ObservableCollection<Operators> Operators { get { return operators; } }

        public OperatorsPage()
        {
            InitializeComponent();
            OperatorsView.ItemsSource = operators;

            InitializeOperators();

            BindingContext = Operators;
            OnPropertyChanged(nameof(Operators));
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            await DisplayAlert("Operators", "This is a list of operators featured in Tom Clancy's Rainbow Six Siege.", "Got it");
        }


        #region Operator initialization
        public void InitializeOperators()
        {
            
            operators.Add(new Operators
            {
                Name = "Attacker - Ash",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/d/d7/Ash_Icon_-_Standard.png/revision/latest?cb=20151222045522",
                Details = "Eliza \"Ash\" Cohen is an Attacking Operator featured in Tom Clancy's Rainbow Six Siege. She is the acting leader of Forward Operations in Rainbow."
            });

            operators.Add(new Operators
            {
                Name = "Attacker - Twitch",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/4/47/Twitch_Badge_New_2.png/revision/latest?cb=20151222045527",
                Details = "Emmanuelle \"Twitch\" Pichon is an Attacking Operator featured in Tom Clancy's Rainbow Six Siege."
            });

            operators.Add(new Operators
            {
                Name = "Attacker - Nokk",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/2/2e/Nokk.png/revision/latest?cb=20190523003359",
                Details = "Nøkk is an Attacking Operator featured in Tom Clancy's Rainbow Six Siege. She was introduced in the Operation Phantom Sight expansion alongside Warden."
            });

            operators.Add(new Operators
            {
                Name = "Defender - Lesion",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/d/d2/R6S-Lesion.png/revision/latest?cb=20170828194022",
                Details = "Liu Tze Long, codenamed Lesion, is a Defending Operator featured in Tom Clancy's Rainbow Six Siege, introduced in the Operation Blood Orchid expansion alongside Ying and Ela"
            });

            operators.Add(new Operators
            {
                Name = "Defender - Doc",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/8/8f/Doc_Badge_2.png/revision/latest?cb=20151222045524",
                Details = "Gustave \"Doc\" Kateb is a Defending Operator featured in Tom Clancy's Rainbow Six Siege"
            });

            operators.Add(new Operators
            {
                Name = "Caveira",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/e/ef/R6S-badge-caveira.png/revision/latest?cb=20160802150554",
                Details = "Taina \"Caveira\" Pereira is a Defending Operator featured in Tom Clancy's Rainbow Six Siege. She was introduced in the Operation Skull Rain expansion alongside Capitão"
            });
        }
        #endregion
    }
}