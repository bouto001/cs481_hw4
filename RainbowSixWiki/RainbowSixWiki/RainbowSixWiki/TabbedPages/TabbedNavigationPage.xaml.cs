﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RainbowSixWiki.TabbedPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedNavigationPage : TabbedPage
    {
        public TabbedNavigationPage()
        {
            InitializeComponent();

            this.Children.Add(new HomePage());
            this.Children.Add(new MapsPage());
            this.Children.Add(new OperatorsPage());
            this.Children.Add(new WeaponsPage());

        }
    }
}