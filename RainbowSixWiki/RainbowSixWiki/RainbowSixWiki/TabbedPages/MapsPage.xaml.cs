﻿using RainbowSixWiki.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RainbowSixWiki.TabbedPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapsPage : ContentPage
    {
        ObservableCollection<Maps> maps = new ObservableCollection<Maps>();
        public ObservableCollection<Maps> Maps { get { return maps; } }

        public MapsPage()
        {
            InitializeComponent();
            MapsView.ItemsSource = maps;

            InitializeMaps();
            
            BindingContext = Maps;
            OnPropertyChanged(nameof(Maps));
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            await DisplayAlert("Maps", "This is a list of maps featured in Tom Clancy's Rainbow Six Siege.", "Got it");
        }

        #region Map initialization
        private void InitializeMaps()
        {
            maps.Add(new Maps
            {
                Name = "Oregon",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/4/48/Siege_Oregon_Thumbnail.PNG/revision/latest/scale-to-width-down/310?cb=20191020064747",
                Details = "Oregon is a map featured in Tom Clancy's Rainbow Six Siege. It is set in an old survivalist compound in Redmond, Oregon. The map is set to be reworked with the release of the Operation Void Edge expansion."
            });

            maps.Add(new Maps
            {
                Name = "House",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/9/9f/House_day.jpg/revision/latest/scale-to-width-down/310?cb=20151023232530",
                Details = "House is a map featured in Tom Clancy's Rainbow Six Siege. It was first shown during the reveal of the game at E3 2014. The map is set to be reworked in the Year 5 Season 2 expansion."
            });

            maps.Add(new Maps
            {
                Name = "Skyscraper",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/a/a0/Screenshot_%2865%29.png/revision/latest/scale-to-width-down/310?cb=20161103173105",
                Details = "Skyscraper is a map featured in Tom Clancy's Rainbow Six Siege that was introduced in the Operation Red Crow expansion"
            });
            maps.Add(new Maps
            {
                Name = "Chalet",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/4/47/Siege_Chalet_Thumbnail.PNG/revision/latest/scale-to-width-down/310?cb=20191020073105",
                Details = "Chalet is a map featured in Tom Clancy's Rainbow Six Siege. The map is set to be reworked in the Year 5 Season 4 expansion."
            });

            maps.Add(new Maps
            {
                Name = "Theme park",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/5/5b/Siege_Theme_Park_Thumbnail.PNG/revision/latest/scale-to-width-down/310?cb=20191020090531",
                Details = "Theme Park is a map featured in Tom Clancy's Rainbow Six Siege that was introduced in the Operation Blood Orchid expansion[1]. The map was later reworked in the Operation Shifting Tides expansion."
            });

            maps.Add(new Maps
            {
                Name = "Kanal",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/6/6e/Siege_Kanal_Thumbnail_2.PNG/revision/latest/scale-to-width-down/310?cb=20191020040558",
                Details = "Kanal is a map featured in Tom Clancy's Rainbow Six Siege, taking place in Hamburg Germany. The map was reworked in September 2019 with the release of Operation Ember Rise"
            });

            maps.Add(new Maps
            {
                Name = "Bank",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/9/99/Siege_Bank_Thumbnail.PNG/revision/latest/scale-to-width-down/310?cb=20191020072134",
                Details = "Bank is a map featured in Tom Clancy's Rainbow Six Siege"
            });

            maps.Add(new Maps
            {
                Name = "Kafe Dostoyevsky",
                Url = "https://vignette.wikia.nocookie.net/rainbowsix/images/7/7e/RainbowSixSiege-KafeDostoyevsky.jpg/revision/latest/scale-to-width-down/310?cb=20151202230016",
                Details = "Kafe Dostoyevsky is a map featured in Tom Clancy's Rainbow Six Siege. The map was reworked in March 2019 with the release of the Operation Phantom Sight expansion. The reworked focused on a massive redesign of the interior of the facility."
            });

        }

        #endregion
    }
}