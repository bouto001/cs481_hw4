﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RainbowSixWiki.ViewModels
{
    public class Operators
    {
        public string Url { get; set; }
        public string Details { get; set; }
        public string Name { get; set; }
    }
}
