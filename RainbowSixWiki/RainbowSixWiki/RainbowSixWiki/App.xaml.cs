﻿using RainbowSixWiki.TabbedPages;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RainbowSixWiki
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new TabbedNavigationPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
